package com.parse.starter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class RegisterActivity extends Activity implements View.OnClickListener{
    private Button cmdReg;
    private AutoCompleteTextView uEmail;
    private EditText uUserName, uPassword, uPasswordAgain;
    private View mProgressView;
    private View mLoginFormView;
    private String userName, userEmail, userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        uUserName = (EditText)findViewById(R.id.editText3);
        uPassword = (EditText)findViewById(R.id.editText);
        uPasswordAgain = (EditText)findViewById(R.id.editText2);
        uEmail = (AutoCompleteTextView)findViewById(R.id.multiAutoCompleteTextView);
        cmdReg = (Button)findViewById(R.id.button2);
        mLoginFormView = findViewById(R.id.email_login_form1);
        mProgressView = findViewById(R.id.progressBar);
        cmdReg.setOnClickListener(this);

    }

    public boolean checkValid(){

        boolean cancel = false;
        View focusView = null;

        uUserName.setError(null);
        uEmail.setError(null);
        uPassword.setError(null);
        uPasswordAgain.setError(null);

        userName = uUserName.getText().toString();
        userEmail = uEmail.getText().toString();
        userPassword = uPassword.getText().toString();
        String userPassword2 = uPasswordAgain.getText().toString();
        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(userName)) {
            uUserName.setError(getString(R.string.error_field_required));
            focusView = uUserName;
            cancel = true;
        }
        if (TextUtils.isEmpty(userPassword)) {
            uPassword.setError(getString(R.string.error_field_required));
            focusView = uPassword;
            cancel = true;
        }
        else if (!isPasswordValid(userPassword)) {
            uPassword.setError(getString(R.string.error_invalid_password));
            focusView = uPassword;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(userPassword2)) {
            uPasswordAgain.setError(getString(R.string.error_field_required));
            focusView = uPasswordAgain;
            cancel = true;
        }
        else if (!userPassword.equals(userPassword2)) {
            uPasswordAgain.setError(getString(R.string.error_invalid_password_again));
            focusView = uPasswordAgain;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(userEmail)) {
            uEmail.setError(getString(R.string.error_field_required));
            focusView = uEmail;
            cancel = true;
        } else if (!isEmailValid(userEmail)) {
            uEmail.setError(getString(R.string.error_invalid_email));
            focusView = uEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            return false;
        } else
            showProgress(true);

            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.

        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private boolean isEmailValid(String userEmail) {
        if(!userEmail.contains("@"))
            return false;
        return true;
    }

    private boolean isPasswordValid(String userPassword) {
        if(userPassword.length() < 5)
            return false;
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if(checkValid()){

            setProgressBarIndeterminateVisibility(true);

            ParseUser newUser = new ParseUser();
            newUser.setUsername(userName);
            newUser.setPassword(userPassword);
            newUser.setEmail(userEmail);
            newUser.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(com.parse.ParseException e) {
                    setProgressBarIndeterminateVisibility(false);

                    if (e == null) {
                        // Success!
                        Intent intent = new Intent(RegisterActivity.this, WorkspaceActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                    else {
                        showProgress(false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setMessage(e.getMessage())
                                .setTitle(R.string.signup_error_title)
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }



            });



        }

    }
}
