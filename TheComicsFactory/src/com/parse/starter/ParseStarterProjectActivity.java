package com.parse.starter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseAnalytics;
import com.parse.ParseCrashReporting;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

public class ParseStarterProjectActivity extends Activity implements View.OnClickListener{
	/** Called when the activity is first created. */
    private Button cmdStart;
    private TextView lblHello;

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        cmdStart = (Button) findViewById(R.id.button3);
        cmdStart.setOnClickListener(this);
        //setParse(this);

        ParseUser currentUser = ParseUser.getCurrentUser();

        if(currentUser == null || currentUser.getUsername() == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        else if (currentUser != null) {
            Intent intent = new Intent(this, WorkspaceActivity.class);
            startActivity(intent);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
    }
    private void setParse(Context context) {
    	ParseCrashReporting.enable(context);

        // Add your initialization code here
	    Parse.initialize(context, "AuPrGr4qNqfWl7nNOKlLiaRJbOS0Z3VkzppelpZt", "CF4JwWrU0rHKD0MwetZwaWwpIOvlSS4WRthmMsgp");
	    //ParseFacebookUtils.initialize("1606609726284851");

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
          
        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true);
        
        ParseACL.setDefaultACL(defaultACL, true);
        
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }
	@Override
    public void onClick(View v) {
        if(v.equals(cmdStart)){
            startActivity(new Intent(this, LoginActivity.class));
        }
    }
}
