package com.parse.starter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


public class WatchFeedActivity extends Activity implements View.OnClickListener{
	private Button cmdLogout, cmdTest;
	private TextView lblHello;
	private Uri imageUri;
	private ImageView img;
	private ParseUser currentUser;
	public static Vector<ParseFile> imagesVec;
	private Vector<ImageView> newVec;
	private final int FULL_SCREEN = 0;
	public int location = 0;

	@Override
	protected  void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_watch_feed);
		currentUser=ParseUser.getCurrentUser();

		newVec = new Vector<ImageView>();

		newVec.add((ImageView)findViewById(R.id.ImageView24));
		newVec.add((ImageView)findViewById(R.id.ImageView23));
		newVec.add((ImageView)findViewById(R.id.ImageView22));
		newVec.add((ImageView)findViewById(R.id.ImageView21));
		newVec.add((ImageView)findViewById(R.id.ImageView21));
		newVec.add((ImageView)findViewById(R.id.ImageView20));
		newVec.add((ImageView)findViewById(R.id.ImageView19));
		newVec.add((ImageView)findViewById(R.id.ImageView18));
		newVec.add((ImageView)findViewById(R.id.ImageView17));
		newVec.add((ImageView)findViewById(R.id.ImageView16));
		newVec.add((ImageView)findViewById(R.id.ImageView15));
		newVec.add((ImageView)findViewById(R.id.ImageView14));
		newVec.add((ImageView)findViewById(R.id.ImageView13));
		newVec.add((ImageView)findViewById(R.id.ImageView12));
		newVec.add((ImageView)findViewById(R.id.ImageView11));
		newVec.add((ImageView)findViewById(R.id.ImageView10));
		newVec.add((ImageView)findViewById(R.id.ImageView09));
		newVec.add((ImageView)findViewById(R.id.ImageView08));
		newVec.add((ImageView)findViewById(R.id.ImageView07));
		newVec.add((ImageView)findViewById(R.id.ImageView06));
		newVec.add((ImageView)findViewById(R.id.ImageView05));
		newVec.add((ImageView)findViewById(R.id.ImageView04));
		newVec.add((ImageView)findViewById(R.id.ImageView03));
		newVec.add((ImageView)findViewById(R.id.ImageView02));
		newVec.add((ImageView)findViewById(R.id.ImageView01));



		imagesVec = new Vector<ParseFile>();
		retrieve();
		getPhotoNumber();





	}

	private void getPhotoNumber(){
		for (int i = 0; i < newVec.size(); i++) {
			

			newVec.get(i).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					for (int i = 0; i < newVec.size(); i++) {
						if(v.equals(newVec.get(i))){
							location = i;
						}
					}
					//					goToDialog();

					//					try {
					//						//								Toast.makeText(getApplication(), 0+"", Toast.LENGTH_LONG).show();
					//						ParseFile d = imagesVec.get(0);
					//						byte data[] = d.getData();
					//
					//						Bitmap bitmap=BitmapFactory.decodeByteArray(data, 0, data.length);
					//
					//						ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					//						bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
					//						byte[] bytes1 = bytes.toByteArray(); 
					//
					//						//
					//						startAct(bytes1);//URI OF IMAGE
					//					} catch (ParseException e) {}


					startAct(location);//URI OF IMAGE
				}
			});
		}
	}

	private String getRealPathFromURI(Uri contentURI) {
		Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file path
			return contentURI.getPath();
		} else { 
			cursor.moveToFirst(); 
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
			return cursor.getString(idx); 
		}
	}

	private void goToDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(WatchFeedActivity.this);
		String options[] = {"Full Screen", "Share"};

		builder.setTitle("Choose an Option:").setItems(options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(which==FULL_SCREEN){//fullScreen
					
				}
				//else
				//share


			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_watch_feed, menu);
		return true;
	}
	private void startAct(int locationOfPhoto){
		Intent intent = new Intent(this ,ShowImageActivity.class);
		intent.putExtra("locationOfPhoto", locationOfPhoto);
		startActivity(intent);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(v.equals(cmdLogout)){

			startActivity(new Intent(this, WorkspaceActivity.class));
		}
		if(v.equals(cmdTest)){
			Toast.makeText(getApplicationContext(), "Not Yet", Toast.LENGTH_LONG).show();
		}

	}

	public synchronized void retrieve(){

		ParseQuery<ParseObject> query = ParseQuery.getQuery("Images");
		query.whereEqualTo("userName", currentUser.getUsername());


		query.findInBackground(new FindCallback<ParseObject>() {	

			@Override
			public void done(List<ParseObject> object, ParseException e) {

				if (e == null) {
					for (ParseObject parseObject : object) {
						ParseFile f = (ParseFile) parseObject.get("image");
						imagesVec.add(f);


					}
				} else {
					Toast.makeText(getApplicationContext(), "wrong **", Toast.LENGTH_LONG).show();
				}


				for (int i = 0; i < newVec.size(); i++) {
					if(imagesVec.isEmpty()){
						continue;
					}
					else if(i<imagesVec.size()){
						try {
							ParseFile d = imagesVec.get(i);
							byte data [] = d.getData();
							Bitmap bitmap=BitmapFactory.decodeByteArray(data, 0, data.length);
							newVec.get(i).setImageBitmap(bitmap);

						} catch (ParseException e1) {}
					}
					else{
						newVec.get(i).setVisibility(View.INVISIBLE);
					}
				}




			}

		});
	}
}
