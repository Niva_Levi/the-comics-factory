package com.parse.starter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.Random;
import java.util.Vector;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.thuytrinh.android.collageviews.MultiTouchListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CreatingComicsActivity extends Activity implements OnClickListener{
	private Button cmdSave, cmdAddIcons, cmdAddText;
	private Uri imageUri, path1;
	private View myGrid;
	private ParseFile photoFile;
	private Vector<View> iconVec;
	private ImageView redFish, pow, redHeart, bubbles, brokenHeart, music, lotsOfBubbles,
		notAgain, hamburgerBubble, chefsHat, devilHorns, scrollWedding, celticBoarder, roses, mustache, pinkMustache,
		glasses, redLoveBirds, 
		baloonBorder, floralBoarder, heartCandyBoarder;
	private EditText bubbleGreen, bubbleYellow, thoughtBubble, bubbleWhite, bubbleBlue, bubbleBlue2;
	private String [] options = {"Red Fish", "Pow", "Red Heart", "Broken Heart", "Bubbles", "Lots Of Bubbles",
			"Music", "Not Again", "hamburgerBubble", "Chef's Hat","Devil horns", "rose", "mustache", "pinkMustache",
			"glasses", "redLoveBirds",
			"scrollWedding", "baloonBorder", "celticBoarder", "floralBoarder", "heartCndyBoarder",  
			"Bubble Right", "Bubble Left",  "Thought Bubble", "bubbleWhite", "bubbleBlue", "bubbleBlue2"};
	private final int START = 21; // where txt bubbles start
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_creating_comics);
		iconVec = new Vector<View>();
		myGrid = (View)findViewById(R.id.myRelativeLayout);
		myGrid.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				return true;
			}
		});
		cmdSave = (Button)findViewById(R.id.button1);
		cmdAddText = (Button)findViewById(R.id.Button02);
		cmdAddIcons = (Button)findViewById(R.id.Button01);
		imageUri=getIntent().getData();
		File f = new File(getRealPathFromURI(imageUri));  
		Drawable d = Drawable.createFromPath(f.getAbsolutePath());
		myGrid.setBackground(d);
		cmdSave.setOnClickListener(this);
		cmdAddText.setOnClickListener(this);
		cmdAddIcons.setOnClickListener(this);
		cmdAddIcons.setText("Add/Remove Icons");
		redFish =(ImageView) findViewById(R.id.imageView1);
		pow =(ImageView) findViewById(R.id.imageView2);
		redHeart =(ImageView) findViewById(R.id.imageView3);
		brokenHeart =(ImageView) findViewById(R.id.imageView4);
		music = (ImageView) findViewById(R.id.ImageView5);
		bubbles =(ImageView) findViewById(R.id.collageView4);
		lotsOfBubbles =(ImageView) findViewById(R.id.imageView6);
		notAgain =(ImageView) findViewById(R.id.imageView7);
		hamburgerBubble =(ImageView) findViewById(R.id.imageView8);
		chefsHat =(ImageView) findViewById(R.id.imageView9);
		devilHorns =(ImageView) findViewById(R.id.imageView10);
		
		
		redLoveBirds = (ImageView) findViewById(R.id.imageView20);
		scrollWedding = (ImageView) findViewById(R.id.imageView11);
		baloonBorder = (ImageView) findViewById(R.id.imageView12);
		celticBoarder = (ImageView) findViewById(R.id.imageView13);
		floralBoarder = (ImageView) findViewById(R.id.imageView14);
		heartCandyBoarder = (ImageView) findViewById(R.id.imageView15);
		roses =(ImageView) findViewById(R.id.imageView16);
		mustache =(ImageView) findViewById(R.id.imageView17);
		pinkMustache =(ImageView) findViewById(R.id.imageView18);
		glasses =(ImageView) findViewById(R.id.imageView19);
		
		bubbleGreen = (EditText)findViewById(R.id.editText1Left);
		bubbleWhite = (EditText)findViewById(R.id.editText2Right2);
		bubbleBlue = (EditText)findViewById(R.id.editText2Right3);
		bubbleBlue2 = (EditText)findViewById(R.id.editText2Right4);
		bubbleYellow = (EditText)findViewById(R.id.editText2Right);
		thoughtBubble = (EditText)findViewById(R.id.editTextThought1);
		

		iconVec.add(redFish);
		iconVec.add(pow);
		iconVec.add(redHeart);
		iconVec.add(brokenHeart);
		iconVec.add(bubbles);
		iconVec.add(lotsOfBubbles);
		iconVec.add(music);
		iconVec.add(notAgain);//7
		iconVec.add(hamburgerBubble);//8
		iconVec.add(chefsHat);//9
		iconVec.add(devilHorns);//10
		
		iconVec.add(roses);//16
		iconVec.add(mustache);//17
		iconVec.add(pinkMustache);//18
		iconVec.add(glasses);//19
		
		
		iconVec.add(redLoveBirds);//20
		
		iconVec.add(scrollWedding);//11
		iconVec.add(baloonBorder);//12
		iconVec.add(celticBoarder);//13
		iconVec.add(floralBoarder);//14
		iconVec.add(heartCandyBoarder);//15
		
		iconVec.add(bubbleGreen);
		iconVec.add(bubbleYellow);
		iconVec.add(thoughtBubble);
		iconVec.add(bubbleWhite);
		iconVec.add(bubbleBlue);
		iconVec.add(bubbleBlue2);
		

		for (View v : iconVec) {

			v.setOnTouchListener(new MultiTouchListener());
			v.setVisibility(View.INVISIBLE);
		}



		








	}

	private String getRealPathFromURI(Uri contentURI) {
		Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file path
			return contentURI.getPath();
		} else { 
			cursor.moveToFirst(); 
			int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
			return cursor.getString(idx); 
		}
	}
	private void saveToInternalSorage(Bitmap bitmapImage){

		if(bitmapImage == null){
			Toast.makeText(getApplicationContext(), "bitmapImage = null", Toast.LENGTH_LONG).show();
			return;
		}
		ContextWrapper context = new ContextWrapper(getApplicationContext());
		//  Get path to new gallery image
		Uri path = context.getContentResolver ().insert (Media.EXTERNAL_CONTENT_URI, new ContentValues());
		path1 = path;
		try
		{
			OutputStream stream = context.getContentResolver ().openOutputStream (path);
			bitmapImage.compress (CompressFormat.JPEG, 100, stream);
		}
		catch (FileNotFoundException e) 
		{
			Toast.makeText(getApplicationContext(), " problem, picture not saved", Toast.LENGTH_LONG).show();
		}

	}
	public Bitmap viewToBitmap(View view) {
		Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);
		return bitmap;
	}
	@Override
	public void onClick(View v) {

		if(v.equals(cmdSave)){
			cmdSave.setVisibility(View.INVISIBLE);
			cmdAddIcons.setVisibility(View.INVISIBLE);
			cmdAddText.setVisibility(View.INVISIBLE);
			Bitmap bm = viewToBitmap(myGrid);
			saveToInternalSorage(bm);
			
			//save to parse
			ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
			bm.compress(Bitmap.CompressFormat.JPEG, 100, stream1);
			byte[] data = stream1.toByteArray();
			saveScaledPhoto(data);

			
			
			Intent intent = new Intent(this ,WorkspaceActivity.class);
//			intent.setData(path1);
			Toast.makeText(getApplicationContext(), "Comics saved!", Toast.LENGTH_SHORT).show();
			startActivity(intent);



			


			
		}
		else if(v.equals(cmdAddIcons)){
			goToDialogAdd();
		}
		
		else if(v.equals(cmdAddText)){
			for (View bubbleText : iconVec) {
				if(bubbleText == iconVec.get(START) || bubbleText == iconVec.get(START+1)
						|| bubbleText == iconVec.get(START +2) || bubbleText == iconVec.get(START +3) 
						|| bubbleText == iconVec.get(START +4)  || bubbleText == iconVec.get(START +5)){
					bubbleText.setOnTouchListener(null);
					bubbleText.setFocusableInTouchMode(true);
					((TextView) bubbleText).setOnEditorActionListener(new EditText.OnEditorActionListener() {
					    @Override
					    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					        if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
					            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
					            
					            
					            ((TextView) v).setOnEditorActionListener(null);
					        	v.setOnTouchListener(new MultiTouchListener());
					        	v.setFocusable(false);
					            return true;
					        }
					        
					        return false;
					    }

					
					});
					
					bubbleText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
					    @Override
					    public void onFocusChange(View v, boolean hasFocus) {
					        if (!hasFocus) {
					        	((TextView) v).setOnEditorActionListener(null);
					        	v.setOnTouchListener(new MultiTouchListener());
					        	v.setFocusable(false);
					        }
					    }
					});
					
				}
				
			}
			
		}
		
	}


	private void goToDialogAdd() {
		AlertDialog.Builder builder = new AlertDialog.Builder(CreatingComicsActivity.this);
		
		builder.setTitle("Choose an Option:").setItems(options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(iconVec.get(which).getVisibility() == View.INVISIBLE)
				iconVec.get(which).setVisibility(View.VISIBLE);
				else
					iconVec.get(which).setVisibility(View.INVISIBLE);
				
			}
		});
		AlertDialog dialog = builder.create();
		dialog.setIcon(R.drawable.red_fish);
		dialog.show();
	}

	
	private void saveScaledPhoto(byte[] data) {

		// Resize photo from camera byte array
		Bitmap snypImage = BitmapFactory.decodeByteArray(data, 0, data.length);
		Bitmap snypImageScaled = Bitmap.createScaledBitmap(snypImage, 700, 700
				* snypImage.getHeight() / snypImage.getWidth(), false);

		// Override Android default landscape orientation and save portrait
		Matrix matrix = new Matrix();
		matrix.postRotate(0);
		Bitmap rotatedScaledMealImage = Bitmap.createBitmap(snypImageScaled, 0,
				0, snypImageScaled.getWidth(), snypImageScaled.getHeight(),
				matrix, true);

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		rotatedScaledMealImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);

		byte[] scaledData = bos.toByteArray();
		Random r = new Random();
		int val = r.nextInt(1000000);
		// Save the scaled image to Parse
		photoFile = new ParseFile(val + "snyp.jpg", scaledData);
		
		
		photoFile.saveInBackground(new SaveCallback() {


			@Override
			public void done(com.parse.ParseException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					ParseUser user = ParseUser.getCurrentUser();
					ParseObject imageSaved = new ParseObject("Images");
					imageSaved.put("image", photoFile);
					imageSaved.put("user", user);
					imageSaved.put("userName", user.getUsername());
					imageSaved.saveInBackground();
//					ParseUser.getCurrentUser().put("photo",photoFile);
//					ParseUser.getCurrentUser().saveInBackground();
					Log.d("save status",photoFile.getName() + " is saved!");
				} else {

					Toast.makeText(getApplication(),
							"Error saving: " + e.getMessage(),
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}



}
