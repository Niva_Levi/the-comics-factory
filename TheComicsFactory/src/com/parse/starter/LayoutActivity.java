package com.parse.starter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import com.thuytrinh.android.collageviews.MultiTouchListener;



import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class LayoutActivity extends Activity implements View.OnClickListener {
	private ImageView cmdPic1, cmdPic2, cmdPic3, cmdPic4;
	private View myScreen ;
	private Button cmdSave, cmdDoneChoosing;
	private Vector<ImageView> imageVector;
	private String selectedImagePath;
	private int index = 0, cameraOrGallery = -1, RESULT_LOAD_IMAGE, count = 0;
	private Uri uriAdd,  path = null;
	private static final int CAMERA = 1, GALLERY = 0, SELECT_PICTURE = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_layout);
		myScreen = (View)findViewById(R.id.myScreen1);
		
		cmdDoneChoosing = (Button)findViewById(R.id.buttonDone);
		cmdSave = (Button)findViewById(R.id.button9);
		
		cmdSave.setVisibility(View.INVISIBLE);
		cmdPic1 = (ImageView)findViewById(R.id.imageButton);
		cmdPic2 = (ImageView)findViewById(R.id.imageButton2);
		cmdPic3 = (ImageView)findViewById(R.id.imageButton3);
		cmdPic4 = (ImageView)findViewById(R.id.imageButton4);

		cmdPic1.setOnClickListener(this);
		cmdPic2.setOnClickListener(this);
		cmdPic3.setOnClickListener(this);
		cmdPic4.setOnClickListener(this);
		cmdDoneChoosing.setOnClickListener(this);
		cmdSave.setOnClickListener(this);
		


		imageVector = new Vector<ImageView>();
		imageVector.add(cmdPic1);
		imageVector.add(cmdPic2);
		imageVector.add(cmdPic3);
		imageVector.add(cmdPic4);



	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_layout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {

		if(v.equals(cmdPic1)){
			index = 0;
			goToDialog();
		}

		else if(v.equals(cmdPic2)){
			index = 1;
			goToDialog();
		}


		else if(v.equals(cmdPic3)){
			index = 2;
			goToDialog();
		}


		else if(v.equals(cmdPic4)){
			index = 3;
			goToDialog();
		}


		else if(v.equals(cmdDoneChoosing)){
			cmdDoneChoosing.setVisibility(View.INVISIBLE);
			cmdSave.setVisibility(View.VISIBLE);
			
			cmdPic1.setOnClickListener(null);
			cmdPic2.setOnClickListener(null);
			cmdPic3.setOnClickListener(null);
			cmdPic4.setOnClickListener(null);
			cmdPic1.setOnTouchListener(new MultiTouchListener());
			cmdPic2.setOnTouchListener(new MultiTouchListener());
			cmdPic3.setOnTouchListener(new MultiTouchListener());
			cmdPic4.setOnTouchListener(new MultiTouchListener());


		}

		else if(v.equals(cmdSave)){
			cmdSave.setVisibility(View.INVISIBLE);
			cmdDoneChoosing.setVisibility(View.INVISIBLE);

			Bitmap bm = viewToBitmap(myScreen);
			saveToInternalSorage(bm);
			Intent intent = new Intent(this ,CreatingComicsActivity.class);
			//			intent.putExtra("imageUri", path.toString());
			intent.setData(path);
			startActivity(intent);




		}

	}

	private void goToDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(LayoutActivity.this);
		String [] options = {"Gallery", "Camera", "Delete Frame"};
		builder.setTitle("Choose an Option:").setItems(options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				cameraOrGallery = which;
				if(which == GALLERY){
					startActivity(ActivityGallery.class);

				}
				else if(which == CAMERA){
					Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(intent,0);
				}
				else{//delete frame
					imageVector.get(index).setVisibility(View.INVISIBLE);
				}
			}
		});
		AlertDialog dialog = builder.create();
		dialog.setIcon(R.drawable.red_fish);
		dialog.show();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(cameraOrGallery == CAMERA) {
			Bitmap bp = (Bitmap) data.getExtras().get("data");
			imageVector.get(index).setImageBitmap(bp);
		}
		//else if(cameraOrGallery == GALLERY){
		else{
			if (resultCode == RESULT_OK) {
				Uri selectedImageUri = data.getData();
				selectedImagePath = getPath(selectedImageUri);
				System.out.println("Image Path : " + selectedImagePath);
				imageVector.get(index).setImageURI(selectedImageUri);
			}
		}

	}
	private void startActivity(final Class<?> activityClass) {
		startActivityForResult(new Intent(this, activityClass), 0);

	}
	public Bitmap viewToBitmap(View view) {
		Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);
		return bitmap;
	}


	private void saveToInternalSorage(Bitmap bitmapImage){

		if(bitmapImage == null){
			Toast.makeText(getApplicationContext(), "bitmapImage = null", Toast.LENGTH_LONG).show();
			return;
		}
		ContextWrapper context = new ContextWrapper(getApplicationContext());
		//  Get path to new gallery image
		path = context.getContentResolver ().insert (Media.EXTERNAL_CONTENT_URI, new ContentValues());
		try
		{
			OutputStream stream = context.getContentResolver ().openOutputStream (path);
			bitmapImage.compress (CompressFormat.JPEG, 100, stream);
		}
		catch (FileNotFoundException e) 
		{
			Toast.makeText(getApplicationContext(), " problem, picture not saved", Toast.LENGTH_LONG).show();
		}

	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
}
