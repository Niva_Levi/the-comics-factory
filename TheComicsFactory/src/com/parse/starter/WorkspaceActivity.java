package com.parse.starter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseAnalytics;
import com.parse.ParseUser;


public class WorkspaceActivity extends Activity implements View.OnClickListener {
    private Button cmdLogout, cmdOStartComics, cmdWatchFeed;
    private TextView lblHello;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workspace);

        cmdLogout = (Button)findViewById(R.id.button4);
        cmdOStartComics = (Button)findViewById(R.id.button5);
        cmdWatchFeed = (Button)findViewById(R.id.button6);

        cmdLogout.setOnClickListener(this);
        cmdOStartComics.setOnClickListener(this);
        cmdWatchFeed.setOnClickListener(this);

        lblHello = (TextView)findViewById(R.id.textView2);
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
        check();

    }

    public void check(){
      ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null ) {
            Log.d("new user", "test");
            lblHello.setText("Hello "+currentUser.getUsername());
        }
        else if(currentUser == null || currentUser.getUsername()==null){
            startActivity(new Intent(this, LoginActivity.class));
        }


    }

    @Override
    public void onClick(View v) {
        if(v.equals(cmdLogout)){
            ParseUser.logOut();
            startActivity(new Intent(this, LoginActivity.class));
        }
        else if(v.equals(cmdOStartComics)){
            startActivity(new Intent(this, LayoutActivity.class));
        }
        else if(v.equals(cmdWatchFeed)){
//            Toast.makeText(getApplicationContext(), "Not Yet", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, WatchFeedActivity.class));
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
